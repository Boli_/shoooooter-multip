
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Realtime;

public class RoomButton : MonoBehaviour
{
    [SerializeField] private TMP_Text texto;
     public RoomInfo info;


    public void SetButtonDetaails(RoomInfo inputinfo)
    {
        info = inputinfo;
        texto.text = info.Name;
    }
    public void JoinRoom()
    {
        Launcher.Instance.JoinRoom(info);
    }
}
