using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_controller : MonoBehaviour
{
    #region Variables
    public Transform gunPoint;
    [SerializeField] internal Camera cam;
    public Transform recoil;
   [SerializeField] private CharacterController cC;
    [SerializeField] private Transform POV;
    [SerializeField] private float runSpeed;
    [SerializeField] private float walkSpeed;
    [SerializeField] private float actualSpeed;
    [SerializeField] private float jumpForce;
    [SerializeField] private float gravityMod;
    [Header("Ground Detection")]
    [SerializeField] private bool isGrounded;
    [SerializeField] private float radio;
    [SerializeField] private float distancia;
    [SerializeField] private Vector3 offset;
    [SerializeField] private LayerMask lm;

    private Vector2 mouseInput;
    private float horizontalRotationStore;
    private float verticalRotationStore;
    private Vector3 direccion;
    private Vector3 movement;
   
    #endregion
    #region Unity Funciones
    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        cam = Camera.main;
    }
    private void Update()
    {
        Rotation();
        Movimiento();
    }
    #endregion
    #region custom Functions

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position + offset, radio);
        if (Physics.SphereCast(transform.position + offset, radio, Vector3.down, out RaycastHit hit, distancia, lm))
        {
            Gizmos.color = Color.magenta;
            Vector3 onPoint = ((transform.position + offset) + (Vector3.down * distancia));
            Gizmos.DrawWireSphere(onPoint, radio);
            Gizmos.DrawLine(transform.position + offset, onPoint);

            Gizmos.DrawSphere(hit.point, 0.1f);
        }
        else
        {
            Gizmos.color = Color.yellow;
            Vector3 onPoint = ((transform.position + offset) + (Vector3.down * distancia));
            Gizmos.DrawWireSphere(onPoint, radio);
            Gizmos.DrawLine(transform.position + offset, onPoint);
        }
    }
    private void LateUpdate()
    {
        cam.transform.position = recoil.transform.position;
        cam.transform.rotation = recoil.transform.rotation; 

        gunPoint.transform.position = recoil.transform.position;
        gunPoint.transform.rotation = recoil.transform.rotation;

    }
    public void Rotation()
    {
        mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        transform.rotation = Quaternion.Euler(0f, mouseInput.x, 0f);
        horizontalRotationStore += mouseInput.x;
        verticalRotationStore -= mouseInput.y;

        verticalRotationStore = Mathf.Clamp(verticalRotationStore,-60F,60F);
        transform.rotation = Quaternion.Euler(0f, horizontalRotationStore, 0f);
        POV.transform.localRotation = Quaternion.Euler(verticalRotationStore, 0f, 0f);
    }

    private void Movimiento()
    {

        direccion = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
        float VelY = movement.y;
        movement = ((transform.forward * direccion.z) + (transform.right * direccion.x)).normalized;
        movement.y = VelY;
        if(Input.GetButton("Fire3"))
        {
            actualSpeed = runSpeed;
        }
        else
        {
            actualSpeed = walkSpeed;
        }
        if (IsGrounded())
        {
            movement.y = 0;
        }
        if (Input.GetButton("Jump")&& cC.isGrounded)
        {
            movement.y = jumpForce * Time.deltaTime;
        }
        movement.y += Physics.gravity.y * Time.deltaTime * gravityMod;
        cC.Move( movement * (actualSpeed * Time.deltaTime  ));
    }
    public bool IsGrounded()
    {
        isGrounded = false;
        if (Physics.SphereCast(transform.position + offset, radio,Vector3.down,out RaycastHit hit, distancia, lm))
        {
            isGrounded = true;
        }
        return isGrounded;
    }
    #endregion
}
