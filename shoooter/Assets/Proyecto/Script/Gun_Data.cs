using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Gun", menuName = "Gun")]
public class Gun_Data : ScriptableObject
{
    public string gunName;
    public int maxMuniCount;
    public int actualMuni;
    public float reloadTime;
    public float damage;
    public float fireRate;
    public float range;
    public Vector2 recoil;

    public bool automatic;
}
