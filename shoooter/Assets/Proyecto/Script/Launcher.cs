using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using Photon.Realtime;

public class Launcher : MonoBehaviourPunCallbacks
{
    public static Launcher Instance;
    [SerializeField] TMP_Text errorText;
    [SerializeField] TMP_Text room;
    [SerializeField] TMP_Text texto; 
    [SerializeField] TMP_Text infoText;
    [SerializeField] TMP_InputField field;
    [SerializeField] GameObject[] screenObjects;
    [SerializeField] GameObject prefBoton;
    [SerializeField] Transform content;
    [SerializeField] List<RoomButton> allRoomButtons = new List<RoomButton>();



    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;  
        }
        else
        {
            Destroy(this);
        }
    }
    private void Start()
    {
        texto.text = "connecting to network...";
        SetScreenObjets(0);
        PhotonNetwork.ConnectUsingSettings();
    }
    public override void OnLeftRoom()
    {
        SetScreenObjets(1);
    }
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        for (int i = 0; i < roomList.Count; i++)
        {
            if (roomList[1].RemovedFromList)
            {
                Debug.Log($"Room Name: {roomList[1].Name}");
                for (int j = 0; j < allRoomButtons.Count; j++)
                {
                    if (roomList[i].Name == allRoomButtons[j].info.Name)
                    {
                        GameObject gb = allRoomButtons[j].gameObject;
                        allRoomButtons.Remove(allRoomButtons[j]);
                    }
                }
            }
            if (roomList[i].PlayerCount!= roomList[i].MaxPlayers&& !roomList[i].RemovedFromList)
            {
                RoomButton newroombuttom = Instantiate(prefBoton, content).GetComponent<RoomButton>();
                newroombuttom.SetButtonDetaails(roomList[i]);
                allRoomButtons.Add(newroombuttom);
            }
          
        }
    }
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
        SetScreenObjets(1);
        
    }
    public override void OnJoinedRoom()
    {
        field.text = PhotonNetwork.CurrentRoom.Name;
        SetScreenObjets(3);
    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        errorText.text = "Esta sala ya existe..." + message;
        SetScreenObjets(4);
    }
    public void Back()
    {
        PhotonNetwork.LeaveRoom();
        SetScreenObjets(1);
    }
    public void SetScreenObjets(int index)
    {
        for (int i = 0; i < screenObjects.Length; i++)
        {
            screenObjects[i].SetActive(i == index);
        }
    }
    public void CreateRoom()
    {
        if (!string.IsNullOrEmpty(field.text))
        {
            RoomOptions roomOptions = new RoomOptions();
            roomOptions.MaxPlayers = 5;
            texto.text = "create room...";
            PhotonNetwork.CreateRoom(field.text);
            SetScreenObjets(2);
        }
    }
    public void JoinRoom(RoomInfo info)
    {
        PhotonNetwork.JoinRoom(info.Name);
        infoText.text = "Join Room...";
        SetScreenObjets(index: 0);
    }
}
