using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class Gun_Controller : MonoBehaviour
{
    #region Variables
   public Player_controller controller;
    public VisualEffect vfx;
    public Camera cam;
    public Gun[] guns;
    public Gun actualGun;

    public int indexGun = 0;
    public int maxGun = 3;

    public GameObject preBullet;
    Vector3 currentRotation;
    Vector3 targetRotation;
    

    float lastShootTime = 0;
    public float returnSpeed;
    public float snapTimes;
    public float lastReload;
    public float changeTime;
    public float lastChingTime;

    private bool reloading;
    private bool isChange;
    #endregion


    #region Funciones
    private void Update()
    {
        if( actualGun != null)
        {
            if (lastShootTime <= 0)
            {
                if (!actualGun.data.automatic)
                {
                    if (Input.GetButtonDown("Fire1"))
                    {
                        if (actualGun.data.actualMuni >= 0)
                        {
                            Shoot();
                        }
                    }
                }
                else
                {
                    if (Input.GetButton("Fire1"))
                    {
                        if (actualGun.data.actualMuni >= 0)
                        {
                            Shoot();
                        }

                    }
                }
            }
            if (Input.GetButtonDown("reload") && !reloading)
            {
                if (actualGun.data.actualMuni<actualGun.data.maxMuniCount)
                {
                    lastReload = 0;
                    reloading = true;
                }
            }
        }
        else
        {

        }

        if(lastShootTime >= 0)
        {
            lastShootTime -= Time.deltaTime;
        }
        if (reloading)
        {
            lastReload += Time.deltaTime;
            if (lastReload >= actualGun.data.reloadTime)
            {
                reloading = false;
                Reload();
            }
        }
        targetRotation = Vector3.Lerp(targetRotation, Vector3.zero, returnSpeed * Time.deltaTime);  
        currentRotation = Vector3.Lerp(currentRotation, targetRotation,snapTimes * Time.deltaTime);
        controller.recoil.localRotation = Quaternion.Euler(currentRotation);
        if (Input.GetButtonDown("Gun1"))
        {
            if (indexGun != 0)
            {
                indexGun = 0;
                lastChingTime = 0;
                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChange = true;
            }
        }
        if (Input.GetButtonDown("Gun2"))
        {
            if (indexGun != 1)
            {
                indexGun = 1;
                lastChingTime = 0;
                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChange = true;
            }
        }
        if (Input.GetButtonDown("Gun3"))
        {
            if (indexGun != 2)
            {
                indexGun = 2;
                lastChingTime = 0;
                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChange = true;
            }
        }
        if (isChange)
        {
            lastChingTime += Time.deltaTime;
            if (lastChingTime >= changeTime)
            {
                isChange = false;
                ChangeGun(indexGun);
            }
        }
    }
    #endregion
    #region Custom function
    public void Shoot()
    {
        if (reloading == false)
        {
        if (Physics.Raycast(controller.cam.transform.position, controller.cam.transform.forward, out RaycastHit hit, actualGun.data.range))
          {
             if(hit.transform != null)
             {
                Debug.Log($"we shoot at: {hit.transform.name}");
                Instantiate(preBullet, hit.point+hit.normal*0.001f, Quaternion.LookRotation(hit.normal, Vector3.up));
             }
          }
        }
            actualGun.data.actualMuni--;
            lastShootTime = actualGun.data.fireRate;
            Recoil();
        GameObject gb = VisualEffect.Instantiate(vfx, actualGun.objetoVcio.position, Quaternion.identity).gameObject;
        gb.transform.parent = actualGun.objetoVcio;
        Destroy(gb, 2f);
    }

    public void Recoil()
    {
       targetRotation -= new Vector3(actualGun.data.recoil.x, Random.Range(-actualGun.data.recoil.y, actualGun.data.recoil.y), 0f);
    }

    public void Reload()
    {
        actualGun.data.actualMuni = actualGun.data.maxMuniCount;
    }
    void ChangeGun(int index)
    {
        if (guns[index]!=null)
        {
            actualGun = guns[index];
            actualGun.gameObject.SetActive(true);
        }
    }
    #endregion

}
